﻿using Unison.Core;
using UnityEngine;

namespace Assets.Test
{
  [UnisonType(ResourcesRootPath = "Test")]
  public class TestUnisonComponent : MonoBehaviour
  {
    protected virtual void Start ()
    {
	    Debug.Log(GetType().FullName);
    }
  }
}
