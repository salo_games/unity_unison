﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unison.Core.Misc;
using UnityEngine;

namespace Unison.Core
{
  [Serializable]
  public sealed class UnisonBaseTypeMeta : UnisonTypeMeta
  {
    public string ResourcesRootPath
    {
      get { return _resourcesRootPath; }
    }

    [SerializeField] private string _resourcesRootPath;
    [SerializeField] private IntUnisonImplementationTypeMetaDictionary _implementationsById;

    public UnisonBaseTypeMeta(int id, string name) : base(id, name)
    {
      _implementationsById = new IntUnisonImplementationTypeMetaDictionary();
    }

    public override bool IsCompilable()
    {
      return base.IsCompilable() && GetImplementationTypes().All(impl => impl.IsCompilable());
    }

    public bool IsCompilableSelf()
    {
      return base.IsCompilable();
    }

    public override bool HasNonCompileErrors()
    {
      return base.HasNonCompileErrors() || GetImplementationTypes().Any(impl => impl.HasNonCompileErrors());
    }

    public bool HasNonCompileErrorsSelf()
    {
      return base.HasNonCompileErrors();
    }

    public IEnumerable<UnisonImplementationTypeMeta> GetImplementationTypes()
    {
      return _implementationsById.Values;
    }

    public int GetImplementationTypesCount()
    {
      return _implementationsById.Count;
    }

    public bool ContainsImplementation(int id)
    {
      return _implementationsById.ContainsKey(id);
    }

    public bool TryGetImplementationById(int id, out UnisonImplementationTypeMeta implementationMeta)
    {
      return _implementationsById.TryGetValue(id, out implementationMeta);
    }

    public int GetNextImplementationTypeId()
    {
      if (_implementationsById.IsNullOrEmpty())
        return 0;

      return _implementationsById.Keys.Max(id => id) + 1;
    }

    public string GetResourcesRootPath()
    {
      return string.IsNullOrEmpty(ResourcesRootPath) ? GetShortName() : ResourcesRootPath;
    }

    public void UpdateResourcesRootPath(string newPath)
    {
      _resourcesRootPath = newPath;
    }

    public void SetImplementationType(int id, string name)
    {
      _implementationsById[id] = new UnisonImplementationTypeMeta(id, name);
    }

    public void RemoveImplementationType(int id)
    {
      _implementationsById.Remove(id);
    }

    public UnisonBaseTypeMeta DirectClone()
    {
      var clone = new UnisonBaseTypeMeta(Id, Name);
      clone.Warnings = Warnings;
      clone.UpdateResourcesRootPath(_resourcesRootPath);

      foreach (var impPair in _implementationsById)
      {
        clone._implementationsById.Add(impPair.Key, impPair.Value.DirectClone());
      }

      return clone;
    }
  }

  [Serializable]
  public class IntUnisonImplementationTypeMetaDictionary : SerializableDictionary<int, UnisonImplementationTypeMeta>
  {
  }
}