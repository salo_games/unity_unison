﻿using UnityEditor;
using UnityEngine;

namespace Unison.Core.Editor
{
  public class UnisonSettingsPopup : PopupWindowContent
  {
    private const int OptionsNum = 3;
    private static readonly Vector2 WindowSize = new Vector2(180f, OptionsNum * 18f + 2f);

    public override Vector2 GetWindowSize()
    {
      return WindowSize;
    }

    public override void OnGUI(Rect rect)
    {
      UnisonSettings.AutoCompile = EditorGUILayout.Toggle("Auto-compile", UnisonSettings.AutoCompile);
      UnisonSettings.LogCompileErrorWarning = EditorGUILayout.Toggle("Log compile error", UnisonSettings.LogCompileErrorWarning);
      UnisonSettings.LogNonCompileErrorWarning = EditorGUILayout.Toggle("Log non-compile error", UnisonSettings.LogNonCompileErrorWarning); 
    }
  }
}
