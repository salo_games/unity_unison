﻿using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Unison.Core.Editor
{
  public abstract class UnisonTypePropertyDrawer : PropertyDrawer
  {
    private int[] _entriesIndices;
    private string[] _entriesNames;
    private SerializedProperty _implementationIdProperty;

    public struct PopupEntry
    {
      public string Name;
      public string Index;

      public PopupEntry(string name, string index)
      {
        Name = name;
        Index = index;
      }
    }

    private int _selectedIndex;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
      EditorGUI.BeginProperty(position, label, property);

      position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

      if (UnisonProjectData.Instance == null)
      {
        EditorGUI.HelpBox(new Rect(position), "No Unison Project Data asset in the project.", MessageType.None);
        EditorGUI.EndProperty();
        return;
      }

      if (!UnisonProjectData.Instance.IsCompilable())
      {
        EditorGUI.HelpBox(new Rect(position), "Unison data is out of sync.", MessageType.None);
        EditorGUI.EndProperty();
        return;
      }

      Initialize(property);

      const float indicatorWidth = 0f;

      var dropdownRect = new Rect(position.x, position.y, position.width - indicatorWidth, position.height);
      var newIndex = EditorGUI.Popup(dropdownRect, _selectedIndex, _entriesNames);

      if (newIndex != _selectedIndex)
      {
        _selectedIndex = newIndex;
        _implementationIdProperty.intValue = _entriesIndices[_selectedIndex];
      }

      EditorGUI.EndProperty();
    }

    private void Initialize(SerializedProperty property)
    {
      var baseTypeId = property.FindPropertyRelative("_baseId").intValue;

      UnisonBaseTypeMeta baseTypeMeta;
      
      if (!UnisonProjectData.Instance.TryGetBaseType(baseTypeId, out baseTypeMeta))
      {
        InitializeAsEmpty();
        return;
      }
      
      var implementationTypeMetas = baseTypeMeta.GetImplementationTypes().ToArray();

      _entriesNames = implementationTypeMetas.Select(data => data.Name.Substring(data.Name.LastIndexOf('.') + 1)).ToArray();
      _entriesIndices = implementationTypeMetas.Select(data => data.Id).ToArray();
      
      _implementationIdProperty = property.FindPropertyRelative("_implementationId");

      for (var i = 0; i < _entriesIndices.Length; i++)
      {
        var entriesIndex = _entriesIndices[i];
        if (entriesIndex == _implementationIdProperty.intValue)
        {
          _selectedIndex = i;
          break;
        }
      }
    }

    private void InitializeAsEmpty()
    {
      _entriesNames = new string[0];
      _entriesIndices = new int[0];
    }
  }
}
