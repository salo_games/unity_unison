﻿using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;

namespace Unison.Core.Editor
{
  public static class UnisonEditor
  {
    public class UnisonEditorAssetPostprocessor : AssetPostprocessor
    {
      private static void OnPostprocessAllAssets(
        string[] importedAssets, string[] deletedAssets, 
        string[] movedAssets, string[] movedFromAssetPaths)
      {
        var data = UnisonProjectData.Instance;

        if (data == null)
          return;

        data.CheckPrefabs();
      }
    }

    private const string AssetName               = UnisonProjectData.ResourceName + ".asset";
    private const string GeneratedCodeName       = "Unison.gen.cs";
    private const string GeneratedEditorCodeName = "UnisonEditor.gen.cs";

    private const string AssetPath               = "/Resources/" + AssetName;
    private const string GeneratedCodePath       = "/Generated/" + GeneratedCodeName;
    private const string GeneratedEditorCodePath = "/Generated/Editor/" + GeneratedEditorCodeName;

    [DidReloadScripts(1000)]
    public static void HandleDidReloadScripts()
    {
      try
      {
        Synchronize();
      }
      catch (Exception e)
      {
        Debug.LogException(e);
      }
    }

    [MenuItem("Assets/Open Unison Data")]
    public static void OpenUnisonProjectDataAsset() 
    {
      var data = GetOrMakeUnisonProjectData();
      Selection.activeObject = data;
    }
    
    public static void Synchronize()
    {
      UnisonProjectData data;

      try
      {
        data = GetOrMakeUnisonProjectData();

        if (data.IsCompilable())
        {
          var unisonTypes = GetUnisonTypes();

          foreach (var unisonType in unisonTypes)
          {
            var implementationTypes = GetImplementationTypes(unisonType);

            foreach (var implementationType in implementationTypes)
            {
              data.TryAdd(unisonType, implementationType.FullName);
            }
          }

          EditorUtility.SetDirty(data);
          AssetDatabase.SaveAssets();

          data.CheckCompilability();
          data.CheckPrefabs();

          if (UnisonSettings.AutoCompile && data.IsCompilable())
            Compile(data);
        }
      }
      finally
      {
        AssetDatabase.Refresh();
      }

      TryPrintWarningToConsole(data);
    }

    public static void TryCompile(UnisonProjectData data)
    {
      if (data == null)
        return;

      if (!data.IsCompilable())
      {
        Debug.LogError("Your data is not compilable.", data);
        return;
      }

      try
      {
        Compile(data);
      }
      catch (Exception e)
      {
        Debug.LogErrorFormat(data, "Some error happened during compilation:\n{0}\n{1}", e.Message, e.StackTrace);
      }

      AssetDatabase.Refresh();
    }

    public static void OverwriteBy(UnisonProjectData newData)
    {
      AssetDatabase.CreateAsset(newData, GetAssetPath());

      UnisonProjectData.Reload();
      UnisonProjectData.Instance.CheckCompilability();
      Synchronize();

      AssetDatabase.Refresh();
    }

    private static void TryPrintWarningToConsole(UnisonProjectData data)
    {
      if (data == null)
        return;

      if (UnisonSettings.LogCompileErrorWarning && !data.IsCompilable())
      {
        Debug.LogError("Unison Project Data is out of sync.", data);
        return;
      }

      if (UnisonSettings.LogNonCompileErrorWarning && data.HasNonCompileErrros())
      {
          Debug.LogWarning("There are some non-compile errors in your Unison Project Data.", data);
      }
    }

    private static UnisonProjectData GetOrMakeUnisonProjectData()
    {
      if (UnisonProjectData.Instance == null)
      {
        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<UnisonProjectData>(), GetAssetPath());
        UnisonProjectData.Reload();
      }

      return UnisonProjectData.Instance;
    }

    private static void Compile(UnisonProjectData data)
    {
      var codeGenerator = new UnisonCodeGenerator(data);
      var code = codeGenerator.GenerateRuntime();
      var editorCode = codeGenerator.GenerateEditor();

      AssetDatabase.StartAssetEditing();

      try
      {
        TryUpdateCode(code);
        TryUpdateEditorCode(editorCode);
      }
      catch (IOException exception)
      {
        throw new UnisonException(string.Format("Generated files saving failed.\nInner exception:\n{0}", exception));
      }

      AssetDatabase.StopAssetEditing();
    }

    private static Type[] GetUnisonTypes()
    {
      return typeof(UnisonTypeAttribute).Assembly.GetTypes().Where(type => typeof(Component).IsAssignableFrom(type) && type.GetCustomAttributes(typeof(UnisonTypeAttribute), false).Any()).ToArray();
    }

    private static Type[] GetImplementationTypes(Type baseType)
    {
      return typeof(UnisonTypeAttribute).Assembly.GetTypes().Where(type => !type.IsAbstract && baseType.IsAssignableFrom(type)).ToArray();
    }

    private static void TryUpdateCode(string code)
    {
      var pathToCode = GetModuleRootFolderPath() + GetGeneratedCodePath();
      TryUpdateFile(pathToCode, code);
    }

    private static void TryUpdateEditorCode(string editorCode)
    {
      var pathToCode = GetModuleRootFolderPath() + GetGeneratedEditorCodePath();
      TryUpdateFile(pathToCode, editorCode);
    }

    private static void TryUpdateFile(string path, string content)
    {
      if (File.Exists(path))
      {
        var oldContent = File.ReadAllText(path);

        if (oldContent.Equals(content))
          return;
      }

      File.WriteAllText(path, content);
    }

    private static string GetModuleRootFolderPath()
    {
      return "Assets/Unison";
    }

    private static string GetAssetPath()
    {
      return GetModuleRootFolderPath() + AssetPath;
    }

    private static string GetGeneratedCodePath()
    {
      return GeneratedCodePath;
    }

    private static string GetGeneratedEditorCodePath()
    {
      return GeneratedEditorCodePath;
    }
  }
}
