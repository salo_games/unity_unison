﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Unison.Core.Editor
{
  public class UnisonCodeGenerator
  {
    private readonly UnisonProjectData _unisonProjectData;

    public UnisonCodeGenerator(UnisonProjectData unisonProjectData)
    {
      _unisonProjectData = unisonProjectData;
    }

    public string GenerateRuntime()
    {
      var sb = new StringBuilder(3);

      AppendBlock(sb, BuildUsings());
      AppendBlock(sb, BuildNamespace(Constants.ModuleName, BuildUBaseEnum()));
      AppendBlock(sb, BuildTypes());

      return sb.ToString();
    }

    public string GenerateEditor()
    {
      var sb = new StringBuilder(2);

      AppendBlock(sb, BuildEditorUsings());
      AppendBlock(sb, BuildEditorTypes());

      return sb.ToString();
    }

    #region Runtime

    private const string Usings = 
@"using System;
using Unison.Core;";

    private static string BuildUsings()
    {
      return Usings;
    }

    private string BuildUBaseEnum()
    {
      const string enumName = "UnisonBaseTypes";
      var enumValues = _unisonProjectData.GetBaseTypes().Select(meta => meta.GetCodeName() + " = " + meta.Id);

      return BuildEnum(enumName, enumValues);
    }

    private static string BuildNamespace(string name, string content)
    {
      return "namespace " + name + "{\n"
              + content
              + "\n}";
    }

    private string BuildTypes()
    {
      var baseTypes = _unisonProjectData.GetBaseTypes();
      var sb = new StringBuilder();

      foreach (var baseType in baseTypes)
      {
        var namespaceName = Constants.ModuleName + "." + baseType.GetCodeName();
        AppendBlock(sb, BuildNamespace(namespaceName, BuildBaseType(baseType)));
      }

      return sb.ToString();
    }

    private const string BaseTypeFormat =
@"[Serializable]
public sealed class {0} : UnisonType
{{
  public {0}() : base({1}) {{ }}

  public {0}({3} implementation) : base({1})
  {{
    Implementation = implementation;
  }}

  public global::{2} LoadPrefab()
  {{
    return LoadPrefab<global::{2}>();
  }}

  public {3} Implementation
  {{
    get
    {{
      return ({3}) ImplementationId; 
    }}
    set
    {{
      ImplementationId = (int) value;
    }}
  }}
}}";

    private static string BuildBaseType(UnisonBaseTypeMeta ubase)
    {
      var sb = new StringBuilder();

      AppendBlock(sb, BuildImplementationTypesEnum(ubase));

      var className = GetBaseTypeClassName(ubase.GetCodeName());
      var id = ubase.Id;
      var impFullName = ubase.Name;
      var enumName = GetImpsEnumName(ubase.GetCodeName());

      var baseTypeClass = string.Format(BaseTypeFormat, className, id, impFullName, enumName);

      sb.Append(baseTypeClass);

      return sb.ToString();
    }

    private static string BuildImplementationTypesEnum(UnisonBaseTypeMeta baseTypeMeta)
    {
      var enumName = GetImpsEnumName(baseTypeMeta.GetCodeName());
      var enumValues = baseTypeMeta.GetImplementationTypes().Select(meta => meta.GetCodeName() + " = " + meta.Id);

      return BuildEnum(enumName, enumValues);
    }

    private static string BuildEnum(string typeName, IEnumerable<string> values)
    {
      var sb = new StringBuilder();

      foreach (var value in values)
      {
        sb.AppendFormat("\n  {0},", value);
      }

      return string.Format("public enum {0}\n{{ {1}\n}}", typeName, sb);
    }

    #endregion Runtime

    #region Editor

    private const string EditorUsings = 
@"using Unison.Core.Editor;
using UnityEditor;";

    private const string EditorImpFormat =
@"[CustomPropertyDrawer(typeof({0}))]
public sealed class {1} : UnisonTypePropertyDrawer {{ }}";

    private static string BuildEditorUsings()
    {
      return EditorUsings;
    }

    private string BuildEditorTypes()
    {
      var baseTypes = _unisonProjectData.GetBaseTypes();
      var sb = new StringBuilder();

      foreach (var baseType in baseTypes)
      {
        AppendBlock(sb, BuildEditorType(baseType));
      }

      return sb.ToString();
    }

    private static string BuildEditorType(UnisonBaseTypeMeta ubase)
    {
      return string.Format(EditorImpFormat,
        GetBaseTypeClassNameAndNamespace(ubase.GetCodeName()),
        GetPropertyDrawerClassName(ubase.GetCodeName()));
    }

    #endregion Editor

    #region Generic

    private static string GetBaseTypeClassName(string baseCodeName)
    {
      return string.Format("{0}UnisonType", baseCodeName);
    }

    private static string GetBaseTypeClassNameAndNamespace(string baseCodeName)
    {
      return string.Format("{1}.{0}.{2}", baseCodeName, Constants.ModuleName, GetBaseTypeClassName(baseCodeName));
    }

    private static string GetPropertyDrawerClassName(string baseCodeName)
    {
      return string.Format("{0}PropertyDrawer", baseCodeName);
    }

    private static string GetImpsEnumName(string baseCodeName)
    {
      return string.Format("{0}UnisonTypes", baseCodeName);
    }

    private static void AppendBlock(StringBuilder sb, string block)
    {
      sb.AppendFormat("{0}\n\n", block);
    }

    #endregion
  }
}