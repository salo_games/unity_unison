﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using E = UnityEditor.EditorGUILayout;
using G = UnityEngine.GUILayout;

namespace Unison.Core.Editor
{
  [CustomEditor(typeof(UnisonProjectData))]
  public class UnisonProjectDataEditor : UnityEditor.Editor
  {
    public class UnisonBaseTypeMetaGuiState
    {
      public int Id;
      public bool Expanded;
      public bool ImplementationsExpanded;
    }

    private const bool BaseTypesDetailsExpandedByDefault = false;
    private const bool ImplementationTypesListExpandedByDefault = true;
    private const float MenuButtonWidth = 50f;
    private const float MenuButtonBigWidth = 70f;
    private const string MetaTypeTitleFormat = "{0}:{1}";

    private UnisonProjectData _target;
    private UnisonProjectData _editTarget;
    private bool _isEditing;
    private readonly Queue<Action> _postDrawActions = new Queue<Action>();
    private readonly List<UnisonBaseTypeMetaGuiState> _baseTypesGuiStates = new List<UnisonBaseTypeMetaGuiState>();
    private Rect _settingsButtonRect;

    public void OnEnable()
    {
      _target = target as UnisonProjectData;

      //  Some sugar
      if (_target.GetBaseTypes().Count() == 1)
      {
        var state = GetStateById(_target.GetBaseTypes().First().Id);
        state.Expanded = true;
      }
    }

    public override void OnInspectorGUI()
    {
      TryInitStyles();

      G.BeginHorizontal();G.EndHorizontal();
      var statusPositionRect = GUILayoutUtility.GetLastRect();
      statusPositionRect.x += 32;
      statusPositionRect.y -= 26;
      DrawHeaderStatus(statusPositionRect);

      E.BeginVertical();
      DrawTopMenu();
      E.Separator();
      DrawUnisonBaseTypes();
      E.EndVertical();

      HandlePostDrawActions();
    }

    private UnisonProjectData CurrentTarget
    {
      get { return _isEditing ? _editTarget : _target; }
    }

    private GUIStyle _errorLabelStyle;
    private GUIStyle _errorFoldoutStyle;
    private GUIStyle _warningLabelStyle;
    private GUIStyle _warningFoldoutStyle;
    private GUIStyle _goodStatusStyle;
    private GUIStyle _badStatusStyle;

    private static readonly Color ErrorText = new Color(1f, 0.1f, 0.1f);
    private static readonly Color WarningText = new Color(0.35f, 0.35f, 0.35f);

    private void TryInitStyles()
    {
      if (_errorLabelStyle == null)
      {
        _errorLabelStyle = new GUIStyle(EditorStyles.label)
        {
          normal = {textColor = ErrorText}
        };
      }

      if (_errorFoldoutStyle == null)
      {
        _errorFoldoutStyle = new GUIStyle("Foldout")
        {
          normal = {textColor = ErrorText}
        };
      }

      if (_warningLabelStyle == null)
      {
        _warningLabelStyle = new GUIStyle(EditorStyles.label)
        {
          normal = {textColor = WarningText}
        };
      }

      if (_warningFoldoutStyle == null)
      {
        _warningFoldoutStyle = new GUIStyle("Foldout")
        {
          normal = {textColor = WarningText}
        };
      }

      if (_goodStatusStyle == null)
      {
        _goodStatusStyle = new GUIStyle(EditorStyles.helpBox)
        {
          alignment = TextAnchor.MiddleCenter,
          normal = {textColor = new Color(0f, 0.2f, 0.9f)}
        };
      }

      if (_badStatusStyle == null)
      {
        _badStatusStyle = new GUIStyle(EditorStyles.helpBox)
        {
          alignment = TextAnchor.MiddleCenter,
          normal = { textColor = new Color(1f, 0.1f, 0.1f) }
        };
      }
    }

    private void HandlePostDrawActions()
    {
      while (_postDrawActions.Count > 0)
      {
        var action = _postDrawActions.Dequeue();

        if (action == null)
          continue;

        action();
      }
    }

    private UnisonBaseTypeMetaGuiState GetStateById(int id)
    {
      if (_baseTypesGuiStates.All(state => state.Id != id))
      {
        _baseTypesGuiStates.Add(new UnisonBaseTypeMetaGuiState
        {
          Id = id,
          Expanded = BaseTypesDetailsExpandedByDefault,
          ImplementationsExpanded = ImplementationTypesListExpandedByDefault
        });
      }

      return _baseTypesGuiStates.First(state => state.Id == id);
    }

    private void SetEditMode()
    {
      _isEditing = true;
      _editTarget = _target.DirectClone();
    }

    private void SetViewMode()
    {
      _isEditing = false;
      _editTarget = null;
    }

    private void ApplyChanges()
    {
      UnisonEditor.OverwriteBy(_editTarget);
      Selection.activeObject = _editTarget;
    }

    #region Draw

    private void DrawTopMenu()
    {
      E.BeginVertical();

      E.BeginHorizontal();


      if (_isEditing)
      {
        if (G.Button("Apply", EditorStyles.miniButtonLeft, G.Width(MenuButtonWidth)))
        {
          ApplyChanges();
          SetViewMode();
        }

        if (G.Button("Cancel", EditorStyles.miniButtonRight, G.Width(MenuButtonWidth)))
        {
          SetViewMode();
        }

        E.LabelField(GUIContent.none);
      }
      else
      {
        if (G.Button("Edit", EditorStyles.miniButtonLeft, G.Width(MenuButtonWidth)))
          SetEditMode();
        if (G.Button("Settings", EditorStyles.miniButtonRight, G.Width(MenuButtonBigWidth)))
          PopupWindow.Show(_settingsButtonRect, new UnisonSettingsPopup());

        if (Event.current.type == EventType.Repaint) _settingsButtonRect = GUILayoutUtility.GetLastRect();

        const string syncButtonText = "Force sync";

        if (!UnisonSettings.AutoCompile)
        {
          if (G.Button(syncButtonText, EditorStyles.miniButtonLeft, G.Width(MenuButtonBigWidth)))
            UnisonEditor.Synchronize();
          if (G.Button("Compile", EditorStyles.miniButtonRight, G.Width(MenuButtonBigWidth)))
            UnisonEditor.TryCompile(_target);
        }
        else
        {
          if (G.Button(syncButtonText, EditorStyles.miniButton, G.Width(MenuButtonBigWidth)))
            UnisonEditor.Synchronize();
        }
      }

      E.EndHorizontal();

      if (_isEditing)
      {
        E.HelpBox("Important! While editing this data consider keeping pairs \"Id -> Type Name\" for types that are already being used in your project.", MessageType.Warning);
        E.HelpBox("If you are not sure about what you are doing google \"unison project data editing\" or try looking on Unity forum. (Will post link here after I create forum post)", MessageType.Info);
      }
      else
      {
        if (!_target.IsCompilable())
        {
          E.HelpBox(
            "Data is out of sync! Fix problem by Editing meta data or your Unison usage (watch hints in foldouts).",
            MessageType.Error);
        }

        if (_target.HasNonCompileErrros())
        {
          E.HelpBox(
            "There are one or more warnings you should see.",
            MessageType.Warning);
        }
      }

      E.EndVertical();
    }

    private void DrawHeaderStatus(Rect r)
    {
      r.width = 120f;
      r.height = 16f;

      string message;
      GUIStyle style;

      if (!_target.IsCompilable())
      {
        message = "Out of sync";
        style = _badStatusStyle;
      }
      else
      {
        message = "Synchronized";
        style = _goodStatusStyle;
      }
      
      GUI.Label(r, message, style);
    }

    private void DrawUnisonBaseTypes()
    {
      foreach (var baseType in CurrentTarget.GetBaseTypes())
      {
        DrawUnisonBaseType(baseType);
      }
    }

    private void DrawUnisonBaseType(UnisonBaseTypeMeta baseType)
    {
      E.BeginVertical();

      E.BeginHorizontal();

      var guiState = GetStateById(baseType.Id);

      var isImplementationsCompilable = baseType.GetImplementationTypes()
        .All(impl => impl.IsCompilable());

      var implementationsHasNonCompileErrors = baseType.GetImplementationTypes().Any(impl => impl.HasNonCompileErrors());

      var isBaseAndImplCompilable = baseType.IsCompilableSelf() && isImplementationsCompilable;
      var baseOrImplHasNonCompileErrors = baseType.HasNonCompileErrorsSelf() || implementationsHasNonCompileErrors;

      var foldoutStyle = !isBaseAndImplCompilable
        ? _errorFoldoutStyle
          : baseOrImplHasNonCompileErrors
          ? _warningFoldoutStyle 
          : "Foldout";

      var title = string.Format(MetaTypeTitleFormat, baseType.GetShortName(), baseType.Id);

      guiState.Expanded = E.Foldout(guiState.Expanded, title, foldoutStyle);

      if (DrawToogleableDeleteButton())
      {
        _postDrawActions.Enqueue(() =>
        {
          CurrentTarget.RemoveBaseType(baseType.Id);
          _baseTypesGuiStates.Remove(guiState);
        });
      }

      E.EndHorizontal();

      if (guiState.Expanded)
      {
        EditorGUI.indentLevel++;
        DrawUnisonBaseTypeDetails(baseType, isImplementationsCompilable, implementationsHasNonCompileErrors);
        EditorGUI.indentLevel--;
      }

      E.EndVertical();
    }

    private void DrawUnisonBaseTypeDetails(UnisonBaseTypeMeta baseTypeMeta, bool isImplemetationsCompilable, bool hasNonCompileErrors)
    {
      E.BeginVertical();

      DrawWarnings(baseTypeMeta.Warnings);

      E.BeginHorizontal();
      E.PrefixLabel("Full name");
      baseTypeMeta.Name = DrawToogleableTextField(baseTypeMeta.Name);
      E.EndHorizontal();

      E.BeginHorizontal();
      E.PrefixLabel("Resources path");
      E.LabelField(baseTypeMeta.GetResourcesRootPath());
      E.EndHorizontal();

      var guiState = GetStateById(baseTypeMeta.Id);

      var foldoutStyle = !isImplemetationsCompilable
        ? _errorFoldoutStyle
          : hasNonCompileErrors
          ? _warningFoldoutStyle
          : "Foldout";

      var implementationsTitle = string.Format("Implementations ({0})", baseTypeMeta.GetImplementationTypesCount());

      guiState.ImplementationsExpanded = E.Foldout(guiState.ImplementationsExpanded,
        implementationsTitle, foldoutStyle);

      if (guiState.ImplementationsExpanded)
      {
        EditorGUI.indentLevel++;
        E.BeginVertical();
        foreach (var implementationType in baseTypeMeta.GetImplementationTypes())
        {
          DrawImplementationType(baseTypeMeta, implementationType);
          E.Separator();
        }
        E.EndVertical();
        EditorGUI.indentLevel--;
      }

      E.EndVertical();
    }

    private void DrawImplementationType(UnisonBaseTypeMeta baseType, UnisonImplementationTypeMeta implementationType)
    {
      E.BeginVertical();

      E.BeginHorizontal();

      var titleStyle = !implementationType.IsCompilable()
        ? _errorLabelStyle
        : implementationType.HasNonCompileErrors()
          ? _warningLabelStyle
          : EditorStyles.label;

      var title = string.Format(MetaTypeTitleFormat, implementationType.GetShortName(), implementationType.Id);

      E.LabelField(title, titleStyle);

      if (DrawToogleableDeleteButton())
      {
        _postDrawActions.Enqueue(() =>
        {
          if (!CurrentTarget.GetBaseTypes().Contains(baseType))
            return;

          baseType.RemoveImplementationType(implementationType.Id);
        });
      }

      E.EndHorizontal();

      EditorGUI.indentLevel++;

      E.BeginVertical();

      DrawWarnings(implementationType.Warnings);

      E.BeginHorizontal();
      E.PrefixLabel("Full name", EditorStyles.miniLabel);
      implementationType.Name = DrawToogleableTextField(implementationType.Name);
      E.EndHorizontal();
      
      var prefabPath = UnisonProjectData.GetPrefabResourcesPath(baseType, implementationType.GetPrefabName());

      E.BeginHorizontal();
      E.PrefixLabel("Prefab path", EditorStyles.miniLabel);
      E.LabelField(prefabPath);
      E.EndHorizontal();

      E.EndVertical();
      EditorGUI.indentLevel--;

      E.EndVertical();
    }

    private static void DrawWarnings(List<UnisonTypeMetaWarningType> warnings)
    {
      foreach (var warning in warnings)
      {
        var warningDetails = UnisonTypeMetaWarnings.Get(warning);
        E.HelpBox(warningDetails.Message, warningDetails.BlocksCompilation ? MessageType.Error : MessageType.Warning);
      }
    }

    private string DrawToogleableTextField(string text)
    {
      if (_isEditing)
      {
        text = E.TextField(text);
      }
      else
      {
        E.LabelField(text);
      }

      return text;
    }

    private bool DrawToogleableDeleteButton()
    {
      if (!_isEditing)
        return false;

      return G.Button("Delete", EditorStyles.miniButton, G.Width(60f));
    }

    #endregion Draw
  }
}
