﻿using UnityEditor;

namespace Unison.Core.Editor
{
  public static class UnisonSettings
  {
    public static bool AutoCompile
    {
      get { return EditorPrefs.GetBool("UnisonSettings_AutoCompile", true); }
      set { EditorPrefs.SetBool("UnisonSettings_AutoCompile", value); }
    }

    public static bool LogCompileErrorWarning
    {
      get { return EditorPrefs.GetBool("UnisonSettings_LogCompileErrorWarning", true); }
      set
      {
        EditorPrefs.SetBool("UnisonSettings_LogCompileErrorWarning", value);

        if (!value)
          LogNonCompileErrorWarning = false;
      }
    }

    public static bool LogNonCompileErrorWarning
    {
      get { return EditorPrefs.GetBool("UnisonSettings_LogNonCompileErrorWarning", true); }
      set
      {
        EditorPrefs.SetBool("UnisonSettings_LogNonCompileErrorWarning", value);
        
        if (value)
          LogCompileErrorWarning = true;
      }
    }
  }
}
