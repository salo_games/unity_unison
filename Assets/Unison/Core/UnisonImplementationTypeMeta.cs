﻿using System;

namespace Unison.Core
{
  [Serializable]
  public sealed class UnisonImplementationTypeMeta : UnisonTypeMeta
  {
    public UnisonImplementationTypeMeta(int id, string name) : base(id, name)
    {
    }

    public string GetPrefabName()
    {
      return GetShortName();
    }

    public UnisonImplementationTypeMeta DirectClone()
    {
      return new UnisonImplementationTypeMeta(Id, Name)
      {
        Warnings = Warnings
      };
    }
  }
}