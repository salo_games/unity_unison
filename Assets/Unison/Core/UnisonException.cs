﻿using System;

namespace Unison.Core
{
  public sealed class UnisonException : Exception
  {
    public UnisonException(string msg) : base (msg)
    {
    }
  }
}
