﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

using Unison.Core.Misc;

namespace Unison.Core
{
  [Serializable]
  public sealed class UnisonProjectData : ScriptableObject
  {
    [SerializeField] private IntUnisonBaseTypeMetaDictionary _unisonBaseTypeMetaById = new IntUnisonBaseTypeMetaDictionary();

    public const string ResourceName = "UnisonProjectData";

    private static UnisonProjectData _instance;

    public static UnisonProjectData Instance
    {
      get
      {
        if (!_instance)
          _instance = GetUnisonData();

        return _instance;
      }
    }

    public static void Reload()
    {
      _instance = GetUnisonData();
    }
    

    public void TryAdd(Type unisonType, string implementationType)
    {
      Assert.IsTrue(unisonType.GetCustomAttributes(typeof(UnisonTypeAttribute), false).Any());

      var unisonTypeFullName = unisonType.FullName;
      var unisonTypeId = GetIdByName(unisonTypeFullName);
      var unisonTypeMeta = unisonTypeId < 0 
        ? InitializeUnisonType(unisonType) 
        : _unisonBaseTypeMetaById[unisonTypeId];

      var rootPathFromAttribute = (unisonType.GetCustomAttributes(typeof(UnisonTypeAttribute), false).First() as UnisonTypeAttribute)
          .ResourcesRootPath;

      unisonTypeMeta.UpdateResourcesRootPath(rootPathFromAttribute);
      
      if (unisonTypeMeta.GetImplementationTypes().Any(data => data.Name.Equals(implementationType)))
        return;

      InitializeImplementationType(unisonTypeMeta, implementationType);
    }

    public void RemoveBaseType(int id)
    {
      _unisonBaseTypeMetaById.Remove(id);
    }

    public void CheckCompilability()
    {
      foreach (var baseTypeMeta in GetBaseTypes())
      {
        baseTypeMeta.ClearCompilationBlockingWarningsSelf();

        foreach (var implementationTypeMeta in baseTypeMeta.GetImplementationTypes())
          implementationTypeMeta.ClearCompilationBlockingWarningsSelf();

        if (GetBaseTypes().Any(meta => meta != baseTypeMeta && meta.Name.Equals(baseTypeMeta.Name)))
        {
          baseTypeMeta.MarkInvalid(UnisonTypeMetaWarningType.NameDuplication);
        }

        var foundBaseType = typeof (UnisonTypeAttribute).Assembly.GetTypes().FirstOrDefault(type => type.FullName.Equals(baseTypeMeta.Name));

        if (foundBaseType == null)
        {
          baseTypeMeta.MarkInvalid(UnisonTypeMetaWarningType.NoTypeFoundByName);
          continue;
        }

        if (!typeof(Component).IsAssignableFrom(foundBaseType))
        {
          baseTypeMeta.MarkInvalid(UnisonTypeMetaWarningType.BaseTypeIsNotAComponent);
          continue;
        }

        if (!foundBaseType.GetCustomAttributes(typeof (UnisonTypeAttribute), false).Any())
        {
          baseTypeMeta.MarkInvalid(UnisonTypeMetaWarningType.BaseTypeDoesntHaveUnisonAttribute);
        }

        if (baseTypeMeta.GetImplementationTypesCount() <= 0)
        {
          baseTypeMeta.MarkInvalid(UnisonTypeMetaWarningType.BaseTypeHasNoImplementations);
        }

        foreach (var implementationTypeMeta in baseTypeMeta.GetImplementationTypes())
        {
          if (
            baseTypeMeta.GetImplementationTypes()
              .Any(meta => meta != implementationTypeMeta && meta.Name.Equals(implementationTypeMeta.Name)))
          {
            implementationTypeMeta.MarkInvalid(UnisonTypeMetaWarningType.NameDuplication);
          }

          var foundImplementationType = typeof(UnisonTypeAttribute).Assembly.GetTypes().FirstOrDefault(type => type.FullName.Equals(implementationTypeMeta.Name));

          if (foundImplementationType == null)
          {
            implementationTypeMeta.MarkInvalid(UnisonTypeMetaWarningType.NoTypeFoundByName);
            continue;
          }

          if (!foundBaseType.IsAssignableFrom(foundImplementationType))
          {
            implementationTypeMeta.MarkInvalid(UnisonTypeMetaWarningType.ImplementationTypeDoesntInheritBaseType);
          }

          if (foundImplementationType.IsAbstract)
          {
            implementationTypeMeta.MarkInvalid(UnisonTypeMetaWarningType.ImplementationTypeIsAbstract);
          }
        }
      }
    }

    public void CheckPrefabs()
    {
      foreach (var baseType in GetBaseTypes())
      {
        baseType.ClearNonCompilationBlockingWarningsSelf();

        foreach (var implementationType in baseType.GetImplementationTypes())
        {
          implementationType.ClearNonCompilationBlockingWarningsSelf();

          var prefabName = implementationType.GetPrefabName();
          var prefabPath = GetPrefabResourcesPath(baseType, prefabName);

          var go = Resources.Load<GameObject>(prefabPath);

          if (!go)
          {
            implementationType.MarkInvalid(UnisonTypeMetaWarningType.NoPrefabByPathInResources);
            continue;
          }

          var implementationComponentType =
            typeof(UnisonTypeAttribute).Assembly.GetTypes()
              .FirstOrDefault(type => type.FullName.Equals(implementationType.Name));

          if (implementationComponentType == null)
            throw new UnisonException(
              string.Format(
                "Couldn't find implementation type \"{0}\" during resources check. This is a bug. Please contact me if this happened.",
                implementationType.Name));

          var component = go.GetComponent(implementationComponentType);

          if (!component)
          {
            implementationType.MarkInvalid(UnisonTypeMetaWarningType.PrefabDoesntHaveImplementationComponentOnIt);
          }
        }}
    }

    public T LoadPrefab<T>(int baseTypeId, int implementationTypeId) where T : Component
    {
      UnisonBaseTypeMeta unisonTypeData;
      
      if (!_unisonBaseTypeMetaById.TryGetValue(baseTypeId, out unisonTypeData))
        throw new UnisonException(string.Format("No Base Type found with id: {0}", baseTypeId));
      
      UnisonImplementationTypeMeta implementationTypeData;
      
      if (!unisonTypeData.TryGetImplementationById(implementationTypeId, out implementationTypeData))
        throw new UnisonException(string.Format("No Implementation Type found in {0}({1}) with id: {2}", unisonTypeData.GetShortName(), baseTypeId, implementationTypeId));

      var prefabName = implementationTypeData.GetPrefabName();
      var prefabPath = GetPrefabResourcesPath(unisonTypeData, prefabName);

      var go = Resources.Load<GameObject>(prefabPath);

      if (!go)
        throw new UnisonException(string.Format("Couldn't load prefab for {0}:{1} by path: {2}.", implementationTypeData.GetShortName(), unisonTypeData.GetShortName(), prefabPath));

      var component = go.GetComponent<T>();

      if (!component)
        throw new UnisonException(string.Format("Loaded prefab doesn't contain {0}:{1}.", implementationTypeData.GetShortName(), unisonTypeData.GetShortName()));

      return component;
    }

    public bool IsCompilable()
    {
      foreach (var baseTypeMeta in GetBaseTypes())
      {
        if (!baseTypeMeta.IsCompilable())
          return false;
      }

      return true;
    }

    public bool HasNonCompileErrros()
    {
      foreach (var baseTypeMeta in GetBaseTypes())
      {
        if (baseTypeMeta.HasNonCompileErrors())
          return true;
      }

      return false;
    }

    public IEnumerable<UnisonBaseTypeMeta> GetBaseTypes()
    {
      return _unisonBaseTypeMetaById.Values;
    }

    public bool TryGetBaseType(int id, out UnisonBaseTypeMeta baseMeta)
    {
      return _unisonBaseTypeMetaById.TryGetValue(id, out baseMeta);
    }

    private static UnisonProjectData GetUnisonData()
    {
      return Resources.Load<UnisonProjectData>(ResourceName);
    }

    private int GetNextBaseTypeId()
    {
      if (_unisonBaseTypeMetaById.IsNullOrEmpty())
        return 0;

      return _unisonBaseTypeMetaById.Keys.Max(id => id) + 1;
    }
    
    private int GetIdByName(string typeName)
    {
      var meta = _unisonBaseTypeMetaById.Values.FirstOrDefault(typeMeta => typeMeta.Name.Equals(typeName));

      if (meta == null)
        return -1;

      return meta.Id;
    }

    private UnisonBaseTypeMeta InitializeUnisonType(Type unisonType)
    {
      var id = GetNextBaseTypeId();
      var meta = new UnisonBaseTypeMeta(id, unisonType.FullName);

      _unisonBaseTypeMetaById[id] = meta;

      return meta;
    }

    public static string GetPrefabResourcesPath(UnisonBaseTypeMeta unisonBaseTypeMeta, string prefabName)
    {
      return string.Format("{0}/{1}", unisonBaseTypeMeta.GetResourcesRootPath(), prefabName);
    }

    private static void InitializeImplementationType(UnisonBaseTypeMeta unisonBaseTypeMeta, string typeName)
    {
      var id = unisonBaseTypeMeta.GetNextImplementationTypeId();
      unisonBaseTypeMeta.SetImplementationType(id, typeName);
    }

    public UnisonProjectData DirectClone()
    {
      var clone = CreateInstance<UnisonProjectData>();
      clone._unisonBaseTypeMetaById = new IntUnisonBaseTypeMetaDictionary();

      foreach (var basePair in _unisonBaseTypeMetaById)
      {
        clone._unisonBaseTypeMetaById.Add(basePair.Key, basePair.Value.DirectClone());
      }

      return clone;
    }
  }

  [Serializable]
  public class IntUnisonBaseTypeMetaDictionary : SerializableDictionary<int, UnisonBaseTypeMeta>
  {
  }
}
