﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Unison.Core.Misc
{
  public static class Extensions
  {
    public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
    {
      return enumerable == null || !enumerable.Any();
    }

    public static IList<T> RemoveWhere<T>(this IList<T> list, Func<T, bool> selector)
    {
      var removeIndices = list.Where(selector).Select((arg1, i) => i).Reverse().ToArray();

      foreach (var removeIndex in removeIndices)
        list.RemoveAt(removeIndex);

      return list;
    }
  }
}
