﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace Unison.Core.Misc
{
  [Serializable]
  public class SerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, ISerializationCallbackReceiver
  {
    [SerializeField]
    private List<TKey> _keys;

    [SerializeField]
    private List<TValue> _values;

    public SerializableDictionary()
    {
      _keys = new List<TKey>();
      _values = new List<TValue>();
    }

    public SerializableDictionary(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
    
    public void OnBeforeSerialize()
    {
      _keys.Clear();
      _values.Clear();

      foreach (var pair in this)
      {
        _keys.Add(pair.Key);
        _values.Add(pair.Value);
      }
    }
    
    public void OnAfterDeserialize()
    {
      Clear();

      if (_keys.Count != _values.Count)
        throw new Exception(string.Format("There are {0} _keys and {1} _values after deserialization. Make sure that both key and value types are serializable.", _keys.Count, _values.Count));

      for (var i = 0; i < _keys.Count; i++)
        Add(_keys[i], _values[i]);
    }
  }
}