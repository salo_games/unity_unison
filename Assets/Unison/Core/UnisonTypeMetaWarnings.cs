﻿using System.Collections.Generic;

namespace Unison.Core
{
  public class UnisonTypeMetaWarning
  {
    public string Message { get; private set; }
    public bool BlocksCompilation { get; private set; }

    public UnisonTypeMetaWarning(bool blocksCompilation, string message)
    {
      BlocksCompilation = blocksCompilation;
      Message = message;
    }
  }

  public static class UnisonTypeMetaWarnings
  {
    private static readonly Dictionary<UnisonTypeMetaWarningType, UnisonTypeMetaWarning> Warnings;

    static UnisonTypeMetaWarnings()
    {
      Warnings = new Dictionary<UnisonTypeMetaWarningType, UnisonTypeMetaWarning>();

#if UNITY_EDITOR
      Warnings[UnisonTypeMetaWarningType.NoTypeFoundByName] = new UnisonTypeMetaWarning(true, "No type was found with the recorded name. Did you rename or delete this type?");
      Warnings[UnisonTypeMetaWarningType.BaseTypeDoesntHaveUnisonAttribute] = new UnisonTypeMetaWarning(true, "Base type doesn't have Unison attribute.");
      Warnings[UnisonTypeMetaWarningType.ImplementationTypeIsAbstract] = new UnisonTypeMetaWarning(true, "Implementation type can not be abstract.");
      Warnings[UnisonTypeMetaWarningType.ImplementationTypeDoesntInheritBaseType] = new UnisonTypeMetaWarning(true, "Implementation type doesn't inherit Base type.");
      Warnings[UnisonTypeMetaWarningType.NameDuplication] = new UnisonTypeMetaWarning(true, "Meta with the same Type name already exists in this scope. Remove wrong duplication.");
      Warnings[UnisonTypeMetaWarningType.BaseTypeHasNoImplementations] = new UnisonTypeMetaWarning(true, "Base type should have at least one implementation or it doesn't make sense.");
      Warnings[UnisonTypeMetaWarningType.BaseTypeIsNotAComponent] = new UnisonTypeMetaWarning(true, "Base type should inherit Component type.");

      Warnings[UnisonTypeMetaWarningType.NoPrefabByPathInResources] = new UnisonTypeMetaWarning(false, "Couldn't find Prefab for this implementation.");
      Warnings[UnisonTypeMetaWarningType.PrefabDoesntHaveImplementationComponentOnIt] = new UnisonTypeMetaWarning(false, "Prefab doesn't have implementation type Component on it.");
#endif
    }

    public static UnisonTypeMetaWarning Get(UnisonTypeMetaWarningType warning)
    {
      UnisonTypeMetaWarning result;
      return Warnings.TryGetValue(warning, out result) ? result : null;
    }
  }
}
