﻿namespace Unison.Core
{
  public enum UnisonTypeMetaWarningType
  {
    NoTypeFoundByName = 1,
    BaseTypeDoesntHaveUnisonAttribute = 2,
    ImplementationTypeIsAbstract = 3,
    ImplementationTypeDoesntInheritBaseType = 4,
    NameDuplication = 5,
    BaseTypeHasNoImplementations = 6,
    BaseTypeIsNotAComponent = 7,

    NoPrefabByPathInResources = 100,
    PrefabDoesntHaveImplementationComponentOnIt = 101,
  }
}