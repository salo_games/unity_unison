﻿using System;

namespace Unison.Core
{
  public class UnisonTypeAttribute : Attribute
  {
    public string ResourcesRootPath { get; set; }
  }
}
