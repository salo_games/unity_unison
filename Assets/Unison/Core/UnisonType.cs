﻿using System;
using UnityEngine;

namespace Unison.Core
{
  [Serializable]
  public abstract class UnisonType
  {
    [SerializeField] private int _baseId;
    [SerializeField] private int _implementationId;

    protected UnisonType(int baseId)
    {
      _baseId = baseId;
    }

    public int BaseId
    {
      get
      {
        return _baseId;
      }
    }

    protected int ImplementationId
    {
      get
      {
        return _implementationId;
      }
      set
      {
        UnisonBaseTypeMeta baseType;
        
        if (!UnisonProjectData.Instance.TryGetBaseType(BaseId, out baseType))
          throw new UnisonException(string.Format("No Base Type found with id: {0}", BaseId));

        if (!baseType.ContainsImplementation(value))
          throw new UnisonException(string.Format("No Implementation Type found in {0}({1}) with id: {2}",
            baseType.GetShortName(), BaseId, value));

        _implementationId = value;
      }
    }

    public T LoadPrefab<T>() where T : Component
    {
      return UnisonProjectData.Instance.LoadPrefab<T>(BaseId, ImplementationId);
    }
  }
}
