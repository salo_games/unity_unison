﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unison.Core.Misc;
using UnityEngine;

namespace Unison.Core
{
  [Serializable]
  public abstract class UnisonTypeMeta
  {
    public int Id
    {
      get { return _id; }
    }

    public string Name
    {
      get { return _name; }
      set { _name = value; }
    }

    public List<UnisonTypeMetaWarningType> Warnings
    {
      get { return _warnings; }
      protected set { _warnings = value; }
    }

    [SerializeField] private int _id;
    [SerializeField] private string _name;
    [SerializeField] private List<UnisonTypeMetaWarningType> _warnings = new List<UnisonTypeMetaWarningType>();

    protected UnisonTypeMeta(int id, string name)
    {
      _id = id;
      _name = name;
    }

    public string GetShortName()
    {
      return Name.Substring(Name.LastIndexOf('.') + 1);
    }

    public string GetCodeName()
    {
      return GetShortName();
    }

    public void ClearCompilationBlockingWarningsSelf()
    {
      _warnings.RemoveWhere(type => UnisonTypeMetaWarnings.Get(type).BlocksCompilation);
    }

    public void ClearNonCompilationBlockingWarningsSelf()
    {
      _warnings.RemoveWhere(type => !UnisonTypeMetaWarnings.Get(type).BlocksCompilation);
    }

    public void MarkInvalid(UnisonTypeMetaWarningType type)
    {
      if (_warnings.Contains(type))
        return;

      _warnings.Add(type);
    }

    public virtual bool IsCompilable()
    {
      return Warnings.All(warning => !UnisonTypeMetaWarnings.Get(warning).BlocksCompilation);
    }

    public virtual bool HasNonCompileErrors()
    {
      return Warnings.Any(warning => !UnisonTypeMetaWarnings.Get(warning).BlocksCompilation);
    }
  }
}
